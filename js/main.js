(function(){
  var target = document.getElementById('twitter_card');
  var buttons =  target.getElementsByClassName('control');
  var m = document.getElementById('news_message_0');
  var items = m.getElementsByTagName('p').length;
  for(var i=0;i<buttons.length;i++){
    buttons[i].addEventListener('click',function(e){
      var f = 590;
      if(this.dataset.direction=='left'){
        f*=-1;
      }
      var margin = m.style.marginLeft || 0 ;
      margin = margin ? parseInt( margin.replace("px")) : margin;
      margin+=f;
      if(margin>0 || (margin < f*(items-1) && f < 0) ){
        return ;
      }
      m.style.marginLeft = margin+'px';
    });
  }

})();